import matplotlib.pyplot as plt
import matplotlib.animation as animation


def plot_status_graph(status_history):
    """
    Plot area graph of infection status over time
    """
    # Unpack data
    infected, notinfected, removed = status_history

    x = range(len(infected))

    # We are actually plotting cummulative totals
    infected_and_notinfected = [i+n for i, n in zip(infected, notinfected)]
    infected_and_notinfected_and_removed = [i+r for i,r in zip(infected_and_notinfected, removed)]

    alpha = 0.3

    fig = plt.figure(figsize=(8, 5))
    ax = fig.add_axes([0, 0, 1, 1])
    ax.plot(x, infected, color='r', label='Infected', alpha=alpha)
    ax.fill_between(x, infected, color='r', alpha=alpha)
    ax.plot(x, infected_and_notinfected, color='b', label='Not Infected', alpha=alpha)
    ax.fill_between(x, infected, infected_and_notinfected, color='b', alpha=alpha)
    ax.plot(x, infected_and_notinfected_and_removed, color='g', label='Removed', alpha=alpha)
    ax.fill_between(x, infected_and_notinfected, infected_and_notinfected_and_removed, color='g', alpha=alpha)
    ax.set_title('Population Status')
    ax.legend(loc='upper right')
    ax.set_ylabel('Population')
    ax.set_xlabel('Time')

    plt.show()

        
def animate_population(data_history):
    """
    Animate movement and infection status of a population over time
    """
    # Unpack data
    x_notinfected_all, y_notinfected_all, x_infected_all, y_infected_all, x_removed_all, y_removed_all = data_history

    # Setup initial graph
    fig = plt.figure(figsize=(8, 5))
    ax = fig.add_axes([0, 0, 1, 1])
    points_notinfected, = ax.plot(x_notinfected_all[0], y_notinfected_all[0], 'o', color='b', label='Not Infected', alpha=0.3)
    points_infected, = ax.plot(x_infected_all[0], y_infected_all[0], 'o', color='r', label='Infected')
    points_removed, = ax.plot(x_removed_all[0], y_removed_all[0], 'o', color='g', label='Removed', alpha=0.3)
    ax.set_title('Population')
    ax.legend(loc='upper right')

    def update(data):
        """
        How to update the graph with the new data you have
        """
        # Unpack data
        x_notinfected, y_notinfected, x_infected, y_infected, x_removed, y_removed = data
        
        # Update x and y axis
        points_notinfected.set_xdata(x_notinfected)
        points_notinfected.set_ydata(y_notinfected)
        points_infected.set_xdata(x_infected)
        points_infected.set_ydata(y_infected)
        points_removed.set_xdata(x_removed)
        points_removed.set_ydata(y_removed)

    def generate_points():
        """
        How to generate new data for each step
        """
        for i in range(len(x_notinfected_all)):
            data = (x_notinfected_all[i], y_notinfected_all[i], x_infected_all[i], y_infected_all[i], x_removed_all[i], y_removed_all[i])
            yield data
    
    # Animate graph over time
    ani = animation.FuncAnimation(fig, update, generate_points, interval=300)
    ani.save('animation_population.gif', writer='imagemagick', fps=10)
    # plt.show()


def evolve_population(Population, time):
    """
    Move a population object through time
    """

    # Move population through time
    infected, notinfected, removed = [0], [Population.population_size], [0]
    x_notinfected_all, y_notinfected_all = [], []
    x_infected_all, y_infected_all = [], []
    x_removed_all, y_removed_all = [], []

    for i in range(time):
        # Update population 1 unit of time
        Population.update()
        
        # Record current status of population
        num_infected, num_notinfected, num_removed = Population.status()
        infected.append(num_infected)
        notinfected.append(num_notinfected)
        removed.append(num_removed)
        
        # Record the position of all people
        x_notinfected, y_notinfected = Population.get_coords(Population.get_notinfected())
        x_notinfected_all.append(x_notinfected)
        y_notinfected_all.append(y_notinfected)
        x_infected, y_infected = Population.get_coords(Population.get_infected())
        x_infected_all.append(x_infected)
        y_infected_all.append(y_infected)
        x_removed, y_removed = Population.get_coords(Population.get_removed())
        x_removed_all.append(x_removed)
        y_removed_all.append(y_removed)
        
    status_history = (infected, notinfected, removed)
    data_history = (x_notinfected_all, y_notinfected_all, x_infected_all, y_infected_all, x_removed_all, y_removed_all)

    return status_history, data_history