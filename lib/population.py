import random
import matplotlib.pyplot as plt
from itertools import product
import numpy as np


class Person:
    """
    Class representing features of a person in a virus simulation
    """

    # Value determining how fine the environment mesh is
    environment_mesh = 10000

    # No one in the population is intialised already removed. Removed here represents either having had the
    # virus and now immune, or have died from the virus
    removed = False

    def __init__(self, prob_init_infected=0.01, infection_radius=0.03, infection_prob=0.3, speed=0.01, max_infected_time=7):
        
        # Assign initial x and y values for location of persion. These are values between 0 and 1
        self.x = random.randrange(0, self.environment_mesh)/self.environment_mesh
        self.y = random.randrange(0, self.environment_mesh)/self.environment_mesh
        
        # Probably that the person is infected when class is initialised
        self.prob_init_infected = prob_init_infected
        
        # Radius at which another person can become infected
        self.infection_radius = infection_radius
        
        # Probability that this person infects a nearby person
        self.infection_prob = infection_prob
        
        # Speed at which the person moves around in the environment
        self.speed = speed
        
        # Time if takes for infected people to be removed
        self.max_infected_time = max_infected_time
        
        # Boolean determining whether the person is infected or not
        self.infected = (random.uniform(0, 1) < self.prob_init_infected)
        
        # Keep track of the amount of time a person has been infected
        self.infected_time = 0 if not self.infected else 1
        
    def within_radius(self, x, y):
        """
        Determine whether a set of coordinates x and y fall within this persons current invection raduis
        """
        
        return ((x - self.x)**2 + (y - self.y)**2) <= self.infection_radius**2
    
    def infect_person(self, person):
        """
        Determine whether another person has been infected by this person
        """
        if self.infected and self.within_radius(person.x, person.y):
            return (random.uniform(0, 1) < self.infection_prob)
        else:
            return False
    
    def move(self):
        """
        Move a person based on their current x and y position and their speed
        """
        
        # Determine new x and y as the current x and y plus -1, 0 or 1 times speed
        new_x = self.x + random.randrange(-1, 2, 1) * self.speed
        new_y = self.y + random.randrange(-1, 2, 1) * self.speed
        
        # Update x and y with new position. Note the new position cannot be greater than 1 or less than 0
        self.x = min(1-random.uniform(0, 0.01), max(0+random.uniform(0, 0.01), new_x))
        self.y = min(1-random.uniform(0, 0.01), max(0+random.uniform(0, 0.01), new_y))
        
    def update_infection(self):
        """
        Update infection time and check whether infected person can be removed from the population
        """
        
        # Update infection time by 1 if the person if infected
        if self.infected:
            self.infected_time += 1
        
        # If the person is infected and their infection time is greater or equal to the maximum infected time then
        # remove them from the population
        if self.infected and self.infected_time >= self.max_infected_time:
            self.infected = False
            self.removed = True


class Population:
    """
    Class representing a group of people in a virus simulation
    """
    
    def __init__(self, population_size=1000, **kwargs):
        
        # The number of people in the population
        self.population_size = population_size
        
        # List of Person objects represent the population
        self.population = [Person(**kwargs) for i in range(self.population_size)]
        
    def __str__(self):
        """
        String represenation of class
        """
        num_infected, num_notinfected, num_removed = self.status()
        
        return f'Population Size: {self.population_size}, Infected: {num_infected}, Not Infected: {num_notinfected}, Removed: {num_removed}'
        
    def plot(self):
        """
        Plot a list of Person objects that represent a population
        """

        # x and y coordinates of people who are not infected
        x_notinfected, y_notinfected = self.get_coords(self.get_notinfected())

        # x and y coordinates of people who are infected
        x_infected, y_infected = self.get_coords(self.get_infected())

        # x and y coordinates of people who are removed
        x_removed, y_removed = self.get_coords(self.get_removed())

        fig = plt.figure(figsize=(8, 5))
        ax = fig.add_axes([0, 0, 1, 1])
        ax.scatter(x_notinfected, y_notinfected, color='b', label='Not Infected', alpha=0.3)
        ax.scatter(x_infected, y_infected, color='r', label='Infected')
        ax.scatter(x_removed, y_removed, color='g', label='Removed', alpha=0.3)
        ax.set_title('Population')
        ax.legend(loc='upper right')

        plt.show()
        
    def get_notinfected(self):
        """
        Get list of infected people
        """
        return [p for p in self.population if not p.infected]
    
    def get_infected(self):
        """
        Get list not infected people
        """        
        return [p for p in self.population if p.infected]
    
    def get_removed(self):
        """
        Get removed people
        """
        return [p for p in self.population if p.removed]
    
    def get_coords(self, people, fillna=True):
        """
        Get list of x and y coordinates 
        """
        x = [p.x for p in people]
        y = [p.y for p in people]
        
        # Fill list with na's up to the size of the population
        
        if fillna:
            x += [np.nan for i in range(self.population_size - len(people))]
            y += [np.nan for i in range(self.population_size - len(people))]
        
        return x, y
        
    def status(self):
        """
        Return the number of people removed, infected and not infected
        """
        num_removed = len([p for p in self.population if p.removed])
        num_notinfected = len([p for p in self.population if not p.infected and not p.removed])
        num_infected = len([p for p in self.population if p.infected])

        return num_infected, num_notinfected, num_removed
    
    def move(self):
        """
        Update each person's x and y coorindate once. This simulates the movement of the population over 1 unit of time|
        """
        for p in self.population:
            p.move()
            
    def infect(self):
        """
        Update the infection status of a population
        """
        # Loop through all possible pairs of people in the population
        for p1, p2 in product(self.population, self.population):
            # If person 1 infects person 2 then update infection state of person 2
            if p1.infect_person(p2):
                p2.infected = True
        
        # Update infection stats for each person
        for p in self.population:
            p.update_infection()
            
    def update(self):
        """
        Update population by one unit of time
        """
        # Move people around
        self.move()
        
        # Infect people
        self.infect()